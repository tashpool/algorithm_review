import { assert } from 'chai'
import { quickSort } from '../src/sort/quickSort.js'

describe('Quick Sort', () => {
  describe('Correct sort', () => {
    it('should return sorted array', () => {
      let inputArray = [64, 26, 12, 45, 22, 11]
      assert.deepEqual(quickSort(inputArray, 0, inputArray.length - 1 ),
        [11, 12, 22, 26, 45, 64])
    })
  })
})