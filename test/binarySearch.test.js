import { assert } from 'chai'
import { binarySearch } from '../src/search/binarySearch.js'

describe('Binary Search', () => {
  describe('Not present', () => {
    it('should return -1 when value is not present', () => {
      assert.equal(binarySearch([4, 6, 9, 14], 3), -1)
    })
  })

  describe('Find Value', () => {
    it('should return index if present', () => {
      assert.equal(binarySearch([4,6,9,14], 6), 1)
    })
  })
})