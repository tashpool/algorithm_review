export function binarySearch(inputArray, targetValue) {
  console.log(`-- Binary Search --`)
  console.log(`Target Value: ${targetValue}`)

  let min = 0
  let max = inputArray.length - 1
  let guess

  while (max >= min) {
    guess = Math.floor((max + min) / 2)
    console.log(`--- === === ---`)
    console.log(`Min: ${min}|Value:${inputArray[min]}, Max: ${max}|Value:${inputArray[max]}`)
    console.log(`Guess: ${guess}|Value:${inputArray[guess]}`)

    if (inputArray[guess] === targetValue) {
      console.log(`Found @ index: ${guess}`)
      return guess
    } else if (inputArray[guess] < targetValue) {
      console.log(`${inputArray[guess]} < ${targetValue}`)
      min = guess + 1
    } else {
      console.log(`${inputArray[guess]} > ${targetValue}`)
      max = guess - 1
    }
  }
  console.log(`Element not found.`)
  return -1
}