function swap(inArray, first, second) {
  let temp = inArray[first]
  inArray[first] = inArray[second]
  inArray[second] = temp
}

// find the partition
function partition(inputArray, low, high) {
  let pivot = inputArray[high]

  // -1 ?
  let i = low - 1

  // transverse
  // compare each with pivot
  for (let j=low; j<=high - 1; j++) {
    if (inputArray[j] < pivot) {
      //
      i++

      // swap element at i with element at j
      swap(inputArray, i, j)
    }
  }

  swap(inputArray, i + 1, high)
  return i + 1
}

export function quickSort(inputArray, low, high) {
  console.log(`\n--- QuickSort Starting ---`)
  console.log(`${inputArray} , low:${low.toString()} high:${high.toString()}`)
  if (low < high) {
    let pi = partition(inputArray, low, high)
    console.log(`Partition: ${pi.toString()}`)

    // rec left
    console.log(`quickSort low:${low.toString()} high:${high.toString()}`)
    quickSort(inputArray, low, pi - 1)

    // rec right
    quickSort(inputArray, pi + 1, high)

    console.log(`Final: ${inputArray}`)
    return inputArray
  }
}