function swap(inArray, first, second) {
  let temp = inArray[first]
  inArray[first] = inArray[second]
  inArray[second] = temp
}

export function selectionSort(inputArray) {
  console.log(`Selection Sort`)
  console.log(`Start: ${inputArray}`)

  let i, j, min_idx
  let arrayLength = inputArray.length

  for (i=0; i<arrayLength - 1; i++) {
    // find min value and set index
    min_idx = i
    console.log(`min_idx: ${min_idx}, value: ${inputArray[min_idx]}`)
    for (j=i + 1; j<arrayLength; j++) {
      console.log(`\tIs ${inputArray[j]} < ${inputArray[min_idx]} ?`)
      if (inputArray[j] < inputArray[min_idx]) {
        console.log(`\tTrue, min_idx=${j}`)
        min_idx = j
      }
    }

    if (min_idx !== i) {
      console.log(`Swap indices ${min_idx} : ${i}`)
      swap(inputArray, min_idx, i)
    }
    console.log(`Inner Loop Done: ${inputArray}`)
  }

  console.log(`\nFinished: ${inputArray}`)
}