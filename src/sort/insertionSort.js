export function insertionSort(inputArray) {
  let i, j, current
  let arrayLength = inputArray.length

  console.log(`\nInsertion Sort`)
  console.log(`Input Array: ${inputArray}`)

  for (i=1; i<arrayLength; i++) {
    console.log(`Current Array ${inputArray}`)
    console.log(`Current index: ${i}, value: ${inputArray[i]}`)
    current = inputArray[i]

    // last element in sub sorted array
    j = i - 1
    console.log(`Last index in sub-sorted array: ${j}`)

    while((j > -1) && (current < inputArray[j])) {
      console.log(`\tSwitch index:${j+1}, Val:${inputArray[j+1]} with index:${j}, Val:${inputArray[j]}`)
      inputArray[j+1] = inputArray[j]
      j--
    }

    console.log(`\tIndex ${j+1} is set to value: ${current}`)
    inputArray[j+1] = current
  }

  // return inputArray
  console.log(inputArray)
}