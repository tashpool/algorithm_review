function merge(inArray, l, m, r) {
  // temp arrays
  // let lowHalf = inArray.slice(l, m)
  // let highHalf = inArray.slice(m+1, r)

  let lowHalf = new Array(m - l + 1)
  let highHalf = new Array(r - m)

  // populate temp array
  for (let x=0; x<lowHalf.length; x++) {
    lowHalf[x] = inArray[l + x]
  }
  for (let y=0; y<highHalf.length; y++) {
    highHalf[y] = inArray[m + 1 + y]
  }

  let lhLength = lowHalf.length
  let rhLength = highHalf.length

  console.log(`\t[ Merge ]`)
  console.log(`\tinArray: ${inArray}, l:${l} m:${m} r:${r}`)
  console.log(`\tlowHalf: ${lowHalf} : len:${lhLength} [comp:${m - l + 1}]`)
  console.log(`\thighHalf: ${highHalf} : len:${rhLength} [comp:${r - m}]`)

  // merge back into inArray[l..r]
  let i = 0
  let j = 0
  let k = l

  // while we have items in BOTH temp arrays, sort them into original array
  while (i < lhLength && j < rhLength) {
    if (lowHalf[i] <= highHalf[j]) {
      inArray[k] = lowHalf[i]
      i++
    } else {
      inArray[k] = highHalf[j]
      j++
    }
    k++
  }

  console.log(`\tAfter comparison merge: ${inArray}`)
  console.log(`\tLeft over indexes i:${i} j:${j} k:${k}`)
  console.log(`\tlowHalf:${lowHalf} , highHalf:${highHalf}`)

  // remaining elements?
  while (i < lhLength) {
    inArray[k] = lowHalf[i]
    i++
    k++
  }

  while (j < rhLength) {
    inArray[k] = highHalf[j]
    j++
    k++
  }

  console.log(`\tFinal Merge: ${inArray}`)
}

export function mergeSort(inArray, l, r) {
  console.log(`\nEntering Merge Sort, l:${l} >= r:${r}`)
  // stop case: left index has caught up to right index
  if (l >= r ) {
    console.log(`Stop Case Reached: ${l} >= ${r}\nReturn.`)
    return
  }

  let m = Math.floor( (l + r) / 2 )
  console.log(`MergeSort Continuing - inArray: ${inArray}, l:${l} m:${m} r:${r}`)

  mergeSort(inArray, l, m)
  mergeSort(inArray, m+1, r)
  merge(inArray, l, m, r)
}
