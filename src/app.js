// const prompt = require('prompt-sync')({sigint: true})
import promptSync from 'prompt-sync'
const prompt = promptSync({sigint: true})
// const fs = require('fs')
import fs from 'fs'
import { binarySearch } from './search/binarySearch.js'
import { selectionSort } from './sort/selectionSort.js'
import { insertionSort } from './sort/insertionSort.js'
import { mergeSort } from './sort/mergeSort.js'
import {quickSort} from './sort/quickSort.js'

const algoMenu = function () {
  console.log('-=-= Algo Start -=-=')
  console.log(`
  1. Binary Search
  2. Selection Sort
  3. Insertion Sort
  4. Merge Sort
  5. Quick Sort
  6. ...
  ctrl + c to exit.
  `)

  return prompt('Which option? ')
}

function jsonReader(filePath, cb) {
  fs.readFile(filePath, 'utf-8', (err, fileData) => {
    if (err) {
      return cb(err)
    }
    try {
      const fileObject = JSON.parse(fileData)
      return cb(null, fileObject)
    } catch (err) {
      return cb(err)
    }
  })
}

let inputData = undefined
inputData = fs.readFileSync('data/testData.json', 'utf-8')

let jsonData = JSON.parse(inputData)
// console.log(jsonData["primeNumbers"])

let choice = algoMenu()
let searchValue

switch (choice) {
  case "1":
    searchValue = prompt('Target value? ')
    binarySearch(jsonData["primeNumbers"], Number(searchValue))
    break
  case "2":
    selectionSort(jsonData["smallSearch"])
    break
  case "3":
    insertionSort(jsonData["sortNumbers"])
    break
  case "4":
    mergeSort(jsonData["smallSearch"], 0, jsonData["smallSearch"].length - 1)
    break
  case "5":
    quickSort(jsonData["smallSearch"], 0, jsonData["smallSearch"].length - 1)
    break
  default:
    console.log(`Don't recognize that choice`)
}

